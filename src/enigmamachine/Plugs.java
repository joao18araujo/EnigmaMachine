/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enigmamachine;

/**
 *
 * @author joao
 */
public class Plugs {
   private final int[] plug;
   
   public Plugs(){
       plug = new int[26];
       for(int i = 0; i < 26; i++){
           plug[i] = i;
       }
   }
   
   public void setOnePlug(int i, int i_plug){
       plug[i] = i_plug;
       plug[i_plug] = i;
       System.out.println("Plugs = ");
       for(int a = 0; a < 26; a++){
           System.out.print(plug[a] + " ");
       }
       System.out.println("lol");
   }
   
   public int[] getPlugs(){
       return this.plug;
   }
   
   public char getOnePlug(char letra){
       char troca =  (char)(plug[(int)(letra-'A')] + 'A');
       
       return troca;
   }
}
