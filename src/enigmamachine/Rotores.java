/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enigmamachine;
import java.util.*;
import enigmamachine.Plugs;
/**
 *
 * @author joao
 */
public final class Rotores {

        public static String rotorI      = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
        public static String rInversoI   = "UWYGADFPVZBECKMTHXSLRINQOJ";
        public static char   ch1         = 'Q';
        
        public static String rotorII     = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
        public static String rInversoII  = "AJPCZWRLFBDKOTYUQGENHXMIVS";
        public static char   ch2         = 'E';
        
        public static String rotorIII    = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
        public static String rInversoIII = "TAGBPCSDQEUFVNZHYIXJWLRKOM";
        public static char   ch3         = 'V';
        
        public static String rotorIV     = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
        public static String rInversoIV  = "HZWVARTNLGUPXQCEJMBSKDYOIF";
        public static char   ch4         = 'J';
        
        public static String rotorV      = "VZBRGITYUPSDNHLXAWMJQOFECK";
        public static String rInversoV   = "QCYLXWENFTZOSMVJUDKGIARPHB";
        public static char   ch5         = 'Z';
                
        public static String reflectorB  = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
        
        private char s1;
        private char s2;
        private char s3;
        
        private String primeiro_rotor;
        private String primeiro_rInverso;
        private char primeiro_ch;
        
        private String segundo_rotor;
        private String segundo_rInverso;
        private char segundo_ch;
        
        private String terceiro_rotor;
        private String terceiro_rInverso;
        private char terceiro_ch;
        
        private int contI;
        private int contII;
        private int contIII;
        
    public Rotores(int i_rotor1, int i_rotor2, int i_rotor3){
        this.s1 = 'A';
        this.s2 = 'A';
        this.s3 = 'A';
        this.setPrimeiroRotor(i_rotor1);
        this.setSegundoRotor(i_rotor2);
        this.setTerceiroRotor(i_rotor3);
    }
    
    public Rotores(){
        this(0, 1, 2);
    }

    public int getContI() {
        return contI;
    }

    public int getContII() {
        return contII;
    }

    public int getContIII() {
        return contIII;
    }
    
    public void setS1(char s1) {
        this.s1 = s1;
    }

    public void setS2(char s2) {
        this.s2 = s2;
    }

    public void setS3(char s3) {
        this.s3 = s3;
    }
       
    public void setPrimeiroRotor(int i_rotor1){
        switch(i_rotor1){
            case 0:
                this.primeiro_rotor = rotorI;
                this.primeiro_rInverso = rInversoI;
                this.primeiro_ch = ch1;
                break;
            case 1:
                this.primeiro_rotor = rotorII;
                this.primeiro_rInverso = rInversoII;
                this.primeiro_ch = ch2;
                break;
            case 2:
                this.primeiro_rotor = rotorIII;
                this.primeiro_rInverso = rInversoIII;
                this.primeiro_ch = ch3;
                break;
            case 3:
                this.primeiro_rotor = rotorIV;
                this.primeiro_rInverso = rInversoIV;
                this.primeiro_ch = ch4;
                break;
            case 4:
                this.primeiro_rotor = rotorV;
                this.primeiro_rInverso = rInversoV;
                this.primeiro_ch = ch5;
                break;
        }
    }
    
    public void setSegundoRotor(int i_rotor2){
        switch(i_rotor2){
            case 0:
                this.segundo_rotor = rotorI;
                this.segundo_rInverso = rInversoI;
                this.segundo_ch = ch1;
                break;
            case 1:
                this.segundo_rotor = rotorII;
                this.segundo_rInverso = rInversoII;
                this.segundo_ch = ch2;
                break;
            case 2:
                this.segundo_rotor = rotorIII;
                this.segundo_rInverso = rInversoIII;
                this.segundo_ch = ch3;
                break;
            case 3:
                this.segundo_rotor = rotorIV;
                this.segundo_rInverso = rInversoIV;
                this.segundo_ch = ch4;
                break;
            case 4:
                this.segundo_rotor = rotorV;
                this.segundo_rInverso = rInversoV;
                this.segundo_ch = ch5;
                break;
        }
    }
    
    public void setTerceiroRotor(int i_rotor3){
        switch(i_rotor3){
            case 0:
                this.terceiro_rotor = rotorI;
                this.terceiro_rInverso = rInversoI;
                this.terceiro_ch = ch1;
                break;
            case 1:
                this.terceiro_rotor = rotorII;
                this.terceiro_rInverso = rInversoII;
                this.terceiro_ch = ch2;
                break;
            case 2:
                this.terceiro_rotor = rotorIII;
                this.terceiro_rInverso = rInversoIII;
                this.terceiro_ch = ch3;
                break;
            case 3:
                this.terceiro_rotor = rotorIV;
                this.terceiro_rInverso = rInversoIV;
                this.terceiro_ch = ch4;
                break;
            case 4:
                this.terceiro_rotor = rotorV;
                this.terceiro_rInverso = rInversoV;
                this.terceiro_ch = ch5;
                break;
        }
    }
    public String rotorFuncionando(String palavra, int[] plugs) {
        // TODO code application logic here
 
        Scanner sc = new Scanner(System.in);
        this.contI = (int)(s1 - 'A');
        this.contII = (int)(s2 - 'A');
        this.contIII = (int)(s3 - 'A');
        System.out.println(contI + " " + contII + " " + contIII);
        String texto;
        ArrayList<Character> resposta = new ArrayList<>(1000);
        
        for(int i = 0; i < palavra.length(); i++){
            contIII++;
            contIII %= 26;

            if(contII == segundo_ch-'A'){
                contII++;
                contII %= 26;
            }

            if(contII == (segundo_ch-'A'+1)%26 && contIII == terceiro_ch-'A'+2){
                contI++;
                contI %= 26;
            }
            
            if(contIII == (terceiro_ch-'A'+1)%26){
                contII++;
                contII %= 26;
                if(contII == (segundo_ch-'A'+1)%26){
                    contI++;
                    contI %= 26;
                }
            }

            System.out.println("_____________________");
            System.out.println((char)(contI+'A') + " " + (char)(contII+'A') + " " + (char)(contIII+'A'));

            int L = (int)(palavra.charAt(i) - 'A');
            L = plugs[L];
            System.out.println("L = " + palavra.charAt(i));
            //System.out.println("L 1 = " + (char)(L+'A'));
            L = transformaLetra(L, contIII, 3);
            //System.out.println("L 2 = " + (char)(L+'A'));
            L = transformaLetra(L, contII, 2);
            //System.out.println("L 3 = " + (char)(L+'A'));
            L = transformaLetra(L, contI, 1);
            //System.out.println("L 4 = " + (char)(L+'A'));
            L = transformaLetra(L, 0, 4);
            //System.out.println("L 5 = " + (char)(L+'A'));
            L = transformaLetra(L, contI, 5);
            //System.out.println("L 6 = " + (char)(L+'A'));
            L = transformaLetra(L, contII, 6);
            //System.out.println("L 7 = " + (char)(L+'A'));
            L = transformaLetra(L, contIII, 7);
            //System.out.println("L 8 = " + (char)(L+'A'));
            while(L < 0) L += 26;
            while(L >= 26) L-=26;
            resposta.add((char)(L+'A'));
        }
        System.out.println(resposta.toString());
        return converteParaString(resposta);
    }
    
    private int transformaLetra(int L, int cont, int rotor){
        while(L < 0) L += 26;
        while(L >= 26) L-= 26;
        int resposta = 0;
        if(rotor == 1) resposta = (int)(primeiro_rotor.charAt((L+cont)%26) - 'A'- cont);
        else if(rotor == 2) resposta = (int)(segundo_rotor.charAt((L+cont)%26) - 'A'- cont);
        else if(rotor == 3) resposta = (int)(terceiro_rotor.charAt((L+cont)%26) - 'A'- cont);
        else if(rotor == 4) resposta = (int)(reflectorB.charAt((L+cont)%26) - 'A'- cont);
        else if(rotor == 5) resposta = (int)(primeiro_rInverso.charAt((L+cont)%26) - 'A'- cont);
        else if(rotor == 6) resposta = (int)(segundo_rInverso.charAt((L+cont)%26) - 'A'- cont);
        else if(rotor == 7)resposta = (int)(terceiro_rInverso.charAt((L+cont)%26) - 'A'- cont);
        while(resposta < 0) resposta += 26;
        while(resposta >= 26) resposta -= 26;
        
        return resposta;
    }
    
    private String converteParaString(ArrayList<Character> resposta){    
        StringBuilder builder = new StringBuilder(resposta.size());
        for(Character c: resposta)
        {
            builder.append(c);
        }
        return builder.toString();
    }
}
